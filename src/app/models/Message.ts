export class Message{
    public id:number;
    public user_id:number;
    public chat_id:number;
    public message:string;
    public readed:number;
    public image:string;
    public file:string;
    public created_at:any;
    public updated_at:any;

    constructor(id,user_id,chat_id,message,readed,image,file,created_at,updated_at){
        this.id=id;
        this.user_id=user_id;
        this.chat_id=chat_id;
        this.message=message;
        this.readed=readed;
        this.image=image;
        this.file=file;
        this.created_at=created_at;
        this.updated_at=updated_at;
    }
}
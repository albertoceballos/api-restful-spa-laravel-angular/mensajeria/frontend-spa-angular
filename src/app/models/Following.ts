export class Following{
    constructor(
        public id:number,
        public user_id:number,
        public followed:number,
        public created_at:any,
        public updated_at:any,
    ){}
}
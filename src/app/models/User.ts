export class User{
    public id:number;
    public role:string;
    public name:string;
    public surname:string;
    public nick:string;
    public email:string;
    public password:string;
    public image:string;
    public created_at:any;
    public updated_at:any;

    constructor(id,role,name,surname,nick,email,password,image,created_at,updated_at){
        this.id=id;
        this.role=role;
        this.name=name;
        this.surname=surname;
        this.nick=nick;
        this.email=email;
        this.password=password;
        this.image=image;
        this.created_at=created_at;
        this.updated_at=updated_at;
    }
}
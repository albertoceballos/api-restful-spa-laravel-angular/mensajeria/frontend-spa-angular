import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';
//modelo necesario para formularios
import {FormsModule} from '@angular/forms';
//módulo peticiones Ajax
import {HttpClientModule} from '@angular/common/http';
//Angular File Uploader
import { AngularFileUploaderModule } from "angular-file-uploader";
//Angular 2 Moment
import {MomentModule} from 'angular2-moment';
//modulo animaciones
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { EditUserComponent } from './components/edit-user/edit-user.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { ContactosComponent } from './components/contactos/contactos.component';
import { ChatsComponent } from './components/chats/chats.component';
import { ShowChatComponent } from './components/show-chat/show-chat.component';
import { NewMessageComponent } from './components/new-message/new-message.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';



//rutas
const appRoutes:Routes=[
  {path:'',component:LoginComponent},
  {path:'login',component:LoginComponent},
  {path:'inicio',component:HomeComponent},
  {path:'registro',component:RegisterComponent},
  {path:'editar-usuario',component:EditUserComponent},
  {path:'usuarios',component:UsuariosComponent},
  {path:'mis-contactos',component:ContactosComponent},
  {path:'mis-chats',component:ChatsComponent},
  {path:'chat/:chatId',component:ShowChatComponent},
  {path:'perfil-usuario/:userId',component:UserProfileComponent},
  {path:'**',component:LoginComponent},
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    EditUserComponent,
    UsuariosComponent,
    ContactosComponent,
    ChatsComponent,
    ShowChatComponent,
    NewMessageComponent,
    UserProfileComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AngularFileUploaderModule,
    MomentModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

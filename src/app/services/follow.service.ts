import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

//url global de la API
import {GLOBAL} from '../services/gobal';

@Injectable({
  providedIn: 'root'
})
export class FollowService {

  public url;
  constructor(private _httpClient:HttpClient) {
    this.url=GLOBAL.url;
   }

   //follows del usuario logueado
   getContacts(token):Observable<any>{
     let headers=new HttpHeaders().set('Authorization',token);
     
     return this._httpClient.get(this.url+'user/contacts',{headers:headers});
   }

   //añadir follow
   addFollow(token,userId):Observable<any>{
     let headers=new HttpHeaders().set('Authorization',token);

     return this._httpClient.get(this.url+'follow/add/'+userId,{headers:headers});
   }

   //borrar follow
   deleteFollow(token,userId):Observable<any>{
     let headers=new HttpHeaders().set('Authorization',token);

     return this._httpClient.get(this.url+'follow/delete/'+userId,{headers:headers});
   }
}


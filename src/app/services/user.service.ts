import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

//url global de la API
import {GLOBAL} from '../services/gobal';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  public url:string;
  constructor(private _httpClient:HttpClient) {
    this.url=GLOBAL.url;
   }

   registerUser(user):Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    let json=JSON.stringify(user);
    let params="json="+json;

    return this._httpClient.post(this.url+'register',params,{headers:headers});
   }

   login(user):Observable<any>{
     let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
     let json=JSON.stringify(user);
     let params="json="+json;
     return this._httpClient.post(this.url+'login',params,{headers:headers});
   }

   update(user,token):Observable<any>{
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded').set('Authorization',token);
    let json=JSON.stringify(user);
    let params="json="+json;
    return this._httpClient.post(this.url+'user/update',params,{headers:headers});

   }

   //todos los usuaraios
   allUsers():Observable<any>{
     return this._httpClient.get(this.url+'all-users');
   }

   //usuario por Id
   getUser(userId):Observable<any>{
      return this._httpClient.get(this.url+'getUser/'+userId);
   }


   //sacar identidad de usuario logueado
   getIdentity(){
     let identity=JSON.parse(localStorage.getItem('identity'));

     return identity;
   }

   //sacr token de usuario logueado
   getToken(){
     let token=localStorage.getItem('token');

     return token;
   }
}

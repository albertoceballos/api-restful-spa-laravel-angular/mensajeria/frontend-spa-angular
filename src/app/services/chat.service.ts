import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

import {GLOBAL} from '../services/gobal';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  public url=GLOBAL.url;
  constructor(private _httpClient:HttpClient) { 
    
  }
  //chats del usuario logueado
  userChats(token):Observable<any>{
    let headers=new HttpHeaders().set('Authorization', token);

    return this._httpClient.get(this.url+'chats',{headers:headers});
  }

  //chat por id de chat
  getChat(chatId,token):Observable<any>{
    let headers=new HttpHeaders().set('Authorization',token);

    return this._httpClient.get(this.url+'chat/'+chatId,{headers:headers});
  }

  //chat por id de contacto
  getChatByUser(userId,token):Observable<any>{
    let headers=new HttpHeaders().set('Authorization',token);

    return this._httpClient.get(this.url+'chatByUser/'+userId,{headers:headers});
  }
}

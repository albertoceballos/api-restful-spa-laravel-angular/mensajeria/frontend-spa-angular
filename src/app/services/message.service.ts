import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http';

import {GLOBAL} from './gobal';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class MessageService {

  public url:string;
  constructor(private _httpClient:HttpClient) {
    this.url=GLOBAL.url;
   }

   //mandar mensaje
   sendMessage(message,token):Observable<any>{
    let headers=new HttpHeaders().set('Authorization',token).set('Content-Type','application/x-www-form-urlencoded');
    let json=JSON.stringify(message);
    let params="json="+json;

    return this._httpClient.post(this.url+'message/send',params,{headers:headers});
   }
}

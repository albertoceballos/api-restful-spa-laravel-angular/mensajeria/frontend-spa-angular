import { Component, OnInit } from '@angular/core';

import { GLOBAL } from '../../services/gobal';
//modelos
import { User } from '../../models/User';
//servicios
import { UserService } from '../../services/user.service';
import { FollowService } from '../../services/follow.service';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css'],
  providers: [UserService, FollowService],
})
export class UsuariosComponent implements OnInit {
  public pageTitle: string;
  public users: Array<User>;
  public contacts;
  public status;
  public message;
  public url;
  public token;
  public identity;
  public followStatus;
  public followMessage;
  public followId;
  constructor(private _userService: UserService, private _followService: FollowService) {
    this.pageTitle = "Usuarios de la red";
    this.token = this._userService.getToken();
    this.identity = this._userService.getIdentity();
    this.url = GLOBAL.url;
    this.contacts = [];
  }

  ngOnInit() {
    this._userService.allUsers().subscribe(
      response => {
        this.status = response.status;
        this.message = response.message;
        if (response.status == 'success') {
          this.users = response.users;
          this.getContacts();
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  getContacts() {
    //sacamos los contactos del usuario logueado
    this._followService.getContacts(this.token).subscribe(
      response => {
        //vacío el array de contactos
        this.contacts=[];
        //meto en un array todos los uauarios a los que sigue el usuario para contrastarlos en la vista
        console.log(response);
        response.follows.forEach(element => {
          this.contacts.push(element.followed);
        });
        console.log(this.contacts);
      },
      error => {
        console.log(error);
      }
    );
  }

  //seguir usuario
  addFollow(userId) {
    console.log(userId);

    this._followService.addFollow(this.token, userId).subscribe(
      response => {
        this.followStatus = response.status;
        if (response.status == 'success') {
          this.getContacts();
          this.followMessage = 'Ahora sigues a este usuario';
          this.followId = userId;
        } else {
          this.followMessage = response.message;
        }
      },
      error => {
        console.log(error);
        this.followStatus = 'error';
      }
    );
  }

  //dejar de seguir usuario
  deleteFollow(userId){
    this._followService.deleteFollow(this.token,userId).subscribe(
      response=>{
        this.followStatus=response.status;
        if(response.status=='success'){
          this.followMessage = 'Has dejado de seguir a este usuario';
          this.followId = userId;
          this.getContacts();
        } else {
          this.followMessage = response.message;
        }
      },
      error=>{
        console.log(error);
        this.followStatus = 'error';
      }
    );
  }


  //Cambiar estilos del botón de Seguir
  addStyle(event) {
    event.target.className = event.target.className.replace('btn-success', 'btn-danger');
    event.target.innerHTML="Dejar de Seguir";
  }

  removeStyle(event) { 
    event.target.className = event.target.className.replace('btn-danger', 'btn-success');
    event.target.innerHTML="Siguiendo";
  }
}

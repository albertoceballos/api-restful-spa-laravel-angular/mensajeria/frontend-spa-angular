import { Component, OnInit } from '@angular/core';

//servcios
import { ChatService } from '../../services/chat.service';
import { UserService} from '../../services/user.service';

//url GLOBAL API
import {GLOBAL} from '../../services/gobal';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.css'],
  providers: [ UserService, ChatService]
})
export class ChatsComponent implements OnInit {
  public pageTitle:string;
  public token:string;
  public url:string;
  public identity;
  public chats;
  public users;
  
  constructor(private _chatService:ChatService, private _userService:UserService) {
    this.pageTitle="Mis Chats";
    this.url=GLOBAL.url;
    this.token=this._userService.getToken();
    this.identity=this._userService.getIdentity();
   }

  ngOnInit() {
    this.userChats();
  }

  userChats(){
    this._chatService.userChats(this.token).subscribe(
      response=>{
        this.chats=response.chats;
        console.log(this.chats);

      },
      error=>{
        console.log(error);
      }
    );
  }

}

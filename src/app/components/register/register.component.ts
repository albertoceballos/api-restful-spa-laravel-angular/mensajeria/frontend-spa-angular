import { Component, OnInit } from '@angular/core';

//modelos
import {User} from '../../models/User';

//servicio
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[UserService]
})
export class RegisterComponent implements OnInit {

  public pageTitle:string;
  public user:User;
  public status:string;
  public message:string;
  constructor(private _userService:UserService) {
    this.pageTitle="Regístrate";
    this.user=new User('','ROLE_USER','','','','','','','','');
   }

  ngOnInit() {
  }

  onSubmit(){
    //llamada al servicio para petición a la API
    this._userService.registerUser(this.user).subscribe(
      response=>{
        this.status=response.status;
        this.message=response.message;
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message="Error al conectar con el servidor";
      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';

import {GLOBAL} from '../../services/gobal';
import {UserService} from '../../services/user.service';
import { FollowService } from '../../services/follow.service';
import { ChatService } from '../../services/chat.service';

import {User} from '../../models/User';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css'],
  providers:[UserService],
})
export class UserProfileComponent implements OnInit {

  public pageTitle:string;
  public url:string;
  public identity;
  public token;
  public status:string;
  public message:string;
  public user:User;
  public contacts:Array<any>;
  public isContact:boolean;
  public followMessage:string;
  public followStatus:string;
  public followId;
  constructor(private _userService:UserService, private _followService:FollowService, private _chatService:ChatService,private _activatedRoute:ActivatedRoute,private _router:Router) {
    this.url=GLOBAL.url;
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.contacts=[];
   }

  ngOnInit() {
    //extraigo el id de usuario de la ruta y se lo paso al método que llama al servicio
    this._activatedRoute.params.subscribe(
      params=>{
        let userId=params['userId'];
        this.getUser(userId);
        this.getContacts();
       
      }
    );
  }

  getUser(userId){
    this._userService.getUser(userId).subscribe(
      response=>{
        this.status=response.status;
        if(response.status=='success'){
          this.user=response.user;
          this.pageTitle="Perfil de "+this.user.name+' '+this.user.surname;
          
        }else{
          this._router.navigate(['inicio']);
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message='Error al conectar con el servidor';
      }
    );
  }

  getContacts() {
    //sacamos los contactos del usuario logueado
    this._followService.getContacts(this.token).subscribe(
      response => {        
        //meto en un array todos los usuarios a los que sigue el usuario para contrastarlos en la vista
        this.contacts=[];
        response.follows.forEach(element => {
          this.contacts.push(element.followed);
        });
        console.log(this.contacts);
        //si el usuario está en ml array de contactos, será contacto sino no
        if(this.contacts.indexOf(this.user.id)!=-1){
          this.isContact=true;
        }else{
          this.isContact=false;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  showChat(userId){
    this._chatService.getChatByUser(userId,this.token).subscribe(
      response=>{
        console.log(response);
        let chatId=response;
        //con el id del chat ya puedo redirigir a la página de chat que corresponda
        this._router.navigate(['chat/',chatId]);
      },
      error=>{
        console.log(error);
      }
    );
  }

  //seguir usuario
  addFollow(userId) {
    console.log(userId);

    this._followService.addFollow(this.token, userId).subscribe(
      response => {
        this.followStatus = response.status;
        if (response.status == 'success') {
          this.getContacts();
          this.followMessage = 'Ahora sigues a este usuario';
          this.followId = userId;
        } else {
          this.followMessage = response.message;
        }
      },
      error => {
        console.log(error);
        this.followStatus = 'error';
      }
    );
  }

  //dejar de seguir usuario
  deleteFollow(userId){
    this._followService.deleteFollow(this.token,userId).subscribe(
      response=>{
        this.followStatus=response.status;
        if(response.status=='success'){
          this.followMessage = 'Has dejado de seguir a este usuario';
          this.followId = userId;
          this.getContacts();
        } else {
          this.followMessage = response.message;
        }
      },
      error=>{
        console.log(error);
        this.followStatus = 'error';
      }
    );
  }

}

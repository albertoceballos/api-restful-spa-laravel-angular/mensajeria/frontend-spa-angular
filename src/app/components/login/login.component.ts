import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
//modelo
import { User } from '../../models/User';

//servicio
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})
export class LoginComponent implements OnInit {

  public pageTitle: string;
  public status: string;
  public message: string;
  public user;
  public identity;
  public token:string;

  constructor(private _userService: UserService, private _router:Router) {
    this.pageTitle = "Login";
    this.user = new User('', '', '', '', '', '', '', '', '', '');
  }

  ngOnInit() {
  }

  onSubmit() {
    this.user.getToken = null;
    this._userService.login(this.user).subscribe(
      response => {
          this.identity = response;
          this.user.getToken = "true";
          this._userService.login(this.user).subscribe(
            response => {
              if(response.status!="error"){
                this.token=response;
                this.status="success";
                this.message="Login correcto";
                console.log(this.identity);
                console.log(this.token);
                //guardar token e identidad en el localStorage
                localStorage.setItem('identity',JSON.stringify(this.identity));
                localStorage.setItem('token',this.token);
                setTimeout(()=>{
                  this._router.navigate(['inicio']);
                },2500);
              }else{
                this.status=response.status;
                this.message=response.message;
              }
            },
            error => {
                console.log(error);
                this.status = 'error'
                this.message = 'Error al conectar con el servidor';
            }
          );
      },
      error => {
        console.log(error);
        this.status = 'error'
        this.message = 'Error al conectar con el servidor';
      }
    );
  }

}

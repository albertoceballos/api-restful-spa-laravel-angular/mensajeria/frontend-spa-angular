import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

//servicios
import { ChatService } from '../../services/chat.service';
import { UserService } from '../../services/user.service';



@Component({
  selector: 'app-show-chat',
  templateUrl: './show-chat.component.html',
  styleUrls: ['./show-chat.component.css'],
  providers: [UserService, ChatService],
})
export class ShowChatComponent implements OnInit {
  public pageTitle;
  public token;
  public identity;
  public status;
  public message;
  public chatMessages;
  public chat;
  public chatId;
  public userName;
  constructor(private _userService: UserService, private _chatService: ChatService,private _router: Router, private _activatedRoute: ActivatedRoute) {
    this.token = this._userService.getToken();
    this.identity=this._userService.getIdentity();


  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        let chatId=params['chatId'];
        this.chatId=chatId;
        this.getChat(chatId);
      }
    );

  }

  getChat(id){
    this._chatService.getChat(id,this.token).subscribe(
      response=>{
        this.status=response.status;
        this.message=response.message;
        if(response.status=='success'){
          this.chatMessages=response.messages;
          this.chat=response.chat;

          //determinar nombre del usuario con el que hablas
          if(this.chat[0].user1.id!=this.identity.sub){
            this.userName=this.chat[0].user1.name + ' ' + this.chat[0].user1.surname;
          }else{
            this.userName=this.chat[0].user2.name + ' ' + this.chat[0].user2.surname;
          }
          this.pageTitle="Chat con "+this.userName;
          console.log(this.chatMessages);
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message='Error al conectar con el servidor';
      }
    );
  }

}

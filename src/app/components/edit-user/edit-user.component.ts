import { Component, OnInit } from '@angular/core';

import {UserService} from '../../services/user.service';

import {User} from '../../models/User';
import {GLOBAL} from '../../services/gobal';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css'],
  providers:[UserService]
})
export class EditUserComponent implements OnInit {
  public pageTitle;
  public status;
  public message;
  public identity;
  public token;
  public user:User;
  public url;
  public afuConfig;
  constructor(private _userService:UserService) {
    this.pageTitle="Editar Usuario";
    this.url=GLOBAL.url;
    this.token=this._userService.getToken();
    this.identity=this._userService.getIdentity();
    this.user=new User(this.identity.sub,'ROLE_USER',this.identity.name,this.identity.surname,this.identity.nick,this.identity.email,'',this.identity.image,'','');

    this.afuConfig = {
      multiple: false,
      formatsAllowed: ".jpg,.png,.jpeg,.gif",
      maxSize: "20",
      uploadAPI: {
        url: this.url + "user/upload-avatar",
        headers: { "Authorization": this.token }
      },
      theme: "attachPin",
      hideProgressBar: false,
      hideResetBtn: true,
      hideSelectBtn: false,
      replaceTexts: {
        selectFileBtn: 'Select Files',
        resetBtn: 'Reset',
        uploadBtn: 'Upload',
        dragNDropBox: 'Drag N Drop',
        attachPinBtn: 'Sube tu foto...',
        afterUploadMsg_success: 'Foto subida con éxito!',
        afterUploadMsg_error: 'Error al subir la foto, inténtalo de nuevo !'
      }
    };


   }


  ngOnInit() {
  }


  onSubmit(){
    this._userService.update(this.user,this.token).subscribe(
      response=>{
        this.status=response.status;
        this.message=response.message;
        console.log(response);
        if(response.status=='success'){
          localStorage.setItem('identity',JSON.stringify(this.user));
        }
      },
      error=>{
        console.log(error);
        this.status='error';
        this.message='Error al conectar con el servidor';
      }
    );
  }


  avatarUpload(data){
    let data_obj=JSON.parse(data.response);
    this.user.image=data_obj.user.image;
    console.log(this.user);
  }
}

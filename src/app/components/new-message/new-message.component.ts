import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { transition, style, animate, trigger } from '@angular/animations';

//modelos
import { Message } from '../../models/Message';

//servicios
import { UserService } from '../../services/user.service';
import { MessageService } from '../../services/message.service';


@Component({
  selector: 'new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.css'],
  providers: [UserService, MessageService],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({ opacity: 0 }),
        animate(300, style({ opacity: 1 }))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(300, style({ opacity: 0 }))
      ])
    ])
  ],
})
export class NewMessageComponent implements OnInit {
  public pageTitle: string;
  public token: string;
  public title: string;
  public message: Message;
  public status: string;
  public statusMessage: string;
  //propiedad del id del chat que me viene del padre:
  @Input() childChatId;

  //llamar al evento del padre
  @Output() eventoPadre = new EventEmitter();

  constructor(private _userService: UserService, private _messageService: MessageService) {
    this.token = this._userService.getToken();
    this.title = "Nuevo Mensaje";
  }

  ngOnInit() {
    this.childChatId = parseInt(this.childChatId);
    this.message = new Message('', '', this.childChatId, '', 0, '', '', '', '');
  }

  //enviar Mensaje
  onSubmit() {
    console.log(this.message);

    this._messageService.sendMessage(this.message, this.token).subscribe(
      response => {
        console.log(response);
        this.status = response.status;
        this.statusMessage = response.message;
        if (response.status == 'success') {
          this.callParent();
          setTimeout(()=>{
            this.status=null;
            this.statusMessage=null;
          },1000);
        }
      },
      error => {
        console.log(error);
        this.status = 'error';
        this.statusMessage = 'Error al enviar el mensaje';
      }
    );
  }

  //método para llamar al evento del padre y refrescar el chat
  callParent() {
    this.eventoPadre.next(this.childChatId);
  }

}
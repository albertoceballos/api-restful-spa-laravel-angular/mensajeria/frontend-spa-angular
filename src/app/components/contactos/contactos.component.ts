import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
//servicios
import {FollowService} from '../../services/follow.service';
import {UserService} from '../../services/user.service';
import {ChatService} from '../../services/chat.service';

//modelos
import {User} from '../../models/User';

import {GLOBAL} from '../../services/gobal';
@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.css'],
  providers: [FollowService, UserService, ChatService],

})
export class ContactosComponent implements OnInit {
  public pageTitle:string;
  public token:string;
  public url:string;
  public followStatus;
  public followMessage;
  public followId;
  public users;
  constructor(private _followService:FollowService, private _userService:UserService, private _chatService:ChatService, private _router:Router) {
    this.pageTitle="Mis Contactos";
    this.token=this._userService.getToken();
    this.url=GLOBAL.url;
    this.users;
   }

  ngOnInit() {
    this.getFollowed();
  }

  getFollowed(){
    this._followService.getContacts(this.token).subscribe(
      response=>{
        if(response.status=='success'){
          this.users=response.users;
          console.log(this.users);
        }
        
      },
      error=>{
        console.log(error);
      }
    );
  }

  deleteFollow(userId){
    this._followService.deleteFollow(this.token,userId).subscribe(
      response=>{
        this.followStatus=response.status;
        if(response.status=='success'){
          this.followMessage = 'Has dejado de seguir a este usuario';
          this.followId = userId;
          this.getFollowed();
        } else {
          this.followMessage = response.message;
        }
      },
      error=>{
        console.log(error);
        this.followStatus = 'error';
      }
    );
  }

  //ir al chat con el contacto
  showChat(userId){
    this._chatService.getChatByUser(userId,this.token).subscribe(
      response=>{
        console.log(response);
        let chatId=response;
        //con el id del chat ya puedo redirigir a la página de chat que corresponda
        this._router.navigate(['chat/',chatId]);
      },
      error=>{
        console.log(error);
      }
    );
  }

}

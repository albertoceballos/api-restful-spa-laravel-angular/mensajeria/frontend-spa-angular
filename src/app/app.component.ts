import { Component,OnInit, DoCheck } from '@angular/core';
import {Router} from '@angular/router';

//servicios
import { UserService} from './services/user.service';
import {GLOBAL} from './services/gobal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService],
})
export class AppComponent implements OnInit, DoCheck{
  public pageTitle;
  public identity;
  public token;
  public url;

  constructor(private _userService:UserService, private _router:Router){
    this.pageTitle= 'mensajeria-laravel';
    this.url=GLOBAL.url;
  }

  ngOnInit(){
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
  }

  ngDoCheck(){
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
  }

  logout(){
    localStorage.clear();
    this.identity=null;
    this.token=null;
    this._router.navigate(['login']);
  }
}
